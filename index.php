<!DOCTYPE html>
<html>
<head>
    <title>My first class</title>
    <link type="text/css" href="style.css" rel="stylesheet">
</head>
<body>
    <p>
        <?php
        class Person{
            public $isAlive = true;
            public $firstname;
            public $lastname;
            public $age;

            public function __construct($firstname, $lastname, $age){
                $this->firstname = $firstname;
                $this->lastname = $lastname;
                $this->age = $age;
            }

            public function greet($firstname, $lastname, $age){
                echo "Hello! My name is " . $this->firstname . $this->lastname . " and I am " . $this->age . ". Nice to meet you!";
            }
        }
        $me = new Person("Taras", "Smile", 20);
        echo $me->greet();
        ?>
    </p>
</body>
</html>
